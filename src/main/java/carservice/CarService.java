package carservice;

import car.Car;
import iterator.HatchbackIterator;
import iterator.PickupIterator;
import iterator.SedanIterator;

import java.util.List;

public interface CarService {
    HatchbackIterator createHatchbackIterator();
    PickupIterator createPickupIterator();
    SedanIterator createSedanIterator();

    int getSedanAmount();
    int getHatchbackAmount();
    int getPickupAmount();

    List<Car> getCars();
}
