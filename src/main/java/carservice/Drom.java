package carservice;

import car.Car;
import car.CarType;
import iterator.HatchbackIterator;
import iterator.PickupIterator;
import iterator.SedanIterator;

import java.util.ArrayList;
import java.util.List;

public class Drom implements CarService{
    private int hatchbackAmount;
    private int pickupAmount;
    private int sedanAmount;
    private List<Car> carList;

    public Drom() {
        carList = new ArrayList<>();
        for(int i = 0; i < 100; i++){
            Car car = new Car();
            if(car.getCarType() == CarType.HATCHBACK){
                hatchbackAmount++;
            }
            if(car.getCarType() == CarType.PICKUP){
                pickupAmount++;
            }
            if(car.getCarType() == CarType.SEDAN){
                sedanAmount++;
            }
            carList.add(car);
        }
    }

    @Override
    public HatchbackIterator createHatchbackIterator() {
        return null;
    }

    @Override
    public PickupIterator createPickupIterator() {
        return null;
    }

    @Override
    public SedanIterator createSedanIterator() {
        return null;
    }

    @Override
    public List<Car> getCars() {
        return carList;
    }

    public int getHatchbackAmount() {
        return hatchbackAmount;
    }

    public int getPickupAmount() {
        return pickupAmount;
    }

    public int getSedanAmount() {
        return sedanAmount;
    }
}
