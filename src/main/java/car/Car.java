package car;

import java.util.Random;

public class Car {
    private String brand;
    private String model;
    private CarType carType;

    public Car(String brand, String model, CarType carType) {
        this.brand = brand;
        this.model = model;
        this.carType = carType;
    }

    public Car() {
        int rand = (int) (Math.random() * 3);
        if(rand == 0){
            carType = CarType.HATCHBACK;
        } else if (rand == 1){
            carType = CarType.PICKUP;
        } else if(rand == 2){
            carType = CarType.SEDAN;
        }
        brand = "";
        model = "";
    }

    public CarType getCarType() {
        return carType;
    }
}
