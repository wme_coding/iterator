package iterator;

import car.Car;

public interface CarIterator {
    boolean hasNext();

    Car getNext();

    void reset();
}
