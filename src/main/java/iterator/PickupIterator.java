package iterator;

import car.Car;
import car.CarType;
import carservice.CarService;

public class PickupIterator implements CarIterator {

    private CarService carService;
    private int currentPos;
    private int currentPickup;

    public PickupIterator(CarService carService) {
        this.carService = carService;
        currentPickup = 0;
        currentPos = 0;
    }

    public boolean hasNext() {
        return carService.getPickupAmount() > currentPickup;
    }

    public Car getNext() {
        for(int i = currentPos + 1; i < carService.getCars().size(); currentPos++){
            if(carService.getCars().get(i).getCarType() == CarType.PICKUP){
                currentPos =  i;
                currentPickup++;
                break;
            }
        }
        return carService.getCars().get(currentPos);
    }

    public void reset() {
        currentPos = 0;
        currentPickup = 0;
    }
}
