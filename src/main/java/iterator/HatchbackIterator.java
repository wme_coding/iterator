package iterator;

import car.Car;
import car.CarType;
import carservice.CarService;

public class HatchbackIterator implements CarIterator{
    private CarService carService;
    private int currentPos;
    private int currentHatchback;

    public HatchbackIterator(CarService carService) {
        this.carService = carService;
        currentHatchback = 0;
        currentPos = 0;
    }

    public boolean hasNext() {
        return carService.getHatchbackAmount() > currentHatchback;
    }

    public Car getNext() {
        for(int i = currentPos + 1; i < carService.getCars().size(); currentPos++){
            if(carService.getCars().get(i).getCarType() == CarType.HATCHBACK){
                currentPos =  i;
                currentHatchback++;
                break;
            }
        }
        return carService.getCars().get(currentPos);
    }

    public void reset() {
        currentPos = 0;
        currentHatchback = 0;
    }
}
