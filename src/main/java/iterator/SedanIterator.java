package iterator;

import car.Car;
import car.CarType;
import carservice.CarService;

public class SedanIterator implements CarIterator{
    private CarService carService;
    private int currentPos;
    private int currentSedan;

    public SedanIterator(CarService carService) {
        this.carService = carService;
        currentSedan = 0;
        currentPos = 0;
    }

    public boolean hasNext() {
        return carService.getHatchbackAmount() > currentSedan;
    }

    public Car getNext() {
        for(int i = currentPos + 1; i < carService.getCars().size(); currentPos++){
            if(carService.getCars().get(i).getCarType() == CarType.SEDAN){
                currentPos =  i;
                currentSedan++;
                break;
            }
        }
        return carService.getCars().get(currentPos);
    }

    public void reset() {
        currentPos = 0;
        currentSedan = 0;
    }
}
